/*remove warning*/
:-style_check(-singleton).
/* parametros Numero elevador, Andar Atual, Status*/
:- dynamic estado_atual/3.

estado_atual(1,1,"p").
estado_atual(2,1,"p").
estado_atual(3,1,"p").

atualiza_andar_atual(Elevador,Andar,Status) :- 
    retract(estado_atual(Elevador,_,_)) , 
    assert(estado_atual(Elevador,Andar,Status)).

qual_elevador(Andar_origem, Andar_destino) :- 
    calculo_elevador(1,Andar_origem,Andar_destino,DistanciaElevador1),
    calculo_elevador(2,Andar_origem,Andar_destino,DistanciaElevador2),
    calculo_elevador(3,Andar_origem,Andar_destino,DistanciaElevador3),
    status_final(Andar_origem,Andar_destino,Status_final),
    calcula_melhor_elevador(DistanciaElevador1,DistanciaElevador2,DistanciaElevador3,R),
    atualiza_andar_atual(R,Andar_destino,Status_final),
    format('Elevador: ~w  Destino: ~wº andar',[R, Andar_destino]).

    
calculo_elevador(Elevador, Andar_origem,Andar_destino,Retorno):-
    estado_atual(Elevador,Andar_atual,Status_atual),
        (
            Andar_origem < Andar_destino,
            Status_atual =\= "d" ->
                calcula_distancia_andares(Andar_atual,Andar_origem,Resultado),
                Retorno is Resultado
            ; 
            (
                Andar_origem > Andar_destino,
                Status_atual =\= "s" ->
                    calcula_distancia_andares(Andar_atual,Andar_origem,Resultado),
                    Retorno is Resultado
                
                ;
                calcula_distancia_andares(Andar_atual,Andar_origem,Resultado),
                Retorno is Resultado+20
            )
        ).

calcula_distancia_andares(Num1,Num2,Resultado):-
(
    Num1 >= Num2 ->
        Resultado is Num1 - Num2
    ;
        Resultado is Num2 - Num1
).

calcula_melhor_elevador(Distancia1,Distancia2,Distancia3,Resultado):-
    (
        Distancia1 =< Distancia2,
        Distancia1 =< Distancia3 ->
            Resultado is 1
        ;
        (
            Distancia2 =< Distancia1,
            Distancia2 =< Distancia3 ->
                Resultado is 2
            ;
            (
                Distancia3 =< Distancia1,
                Distancia3 =< Distancia2 ->
                    Resultado is 3
                
                    
            )         
        )       
    ).

status_final(Andar_origem,Andar_destino,Resultado):-
    (
        Andar_origem > Andar_destino ->
            Resultado = "d"
        ;
            Resultado = "s"
    ).